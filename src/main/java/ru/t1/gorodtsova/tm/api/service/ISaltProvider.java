package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
